# Docker

## Einfache Befehle

### Container starten
```
docker run [IMAGE]
```

### Container im Hintergrund starten
```
docker run -d [IMAGE]
```

### Alle laufenden Container anzeigen:
```
docker ps
```

### Container im Betrieb entfernen
```
docker kill [CONTAINER-ID]
```
Die **CONTAINER-ID** bekommt man über **docker ps**

### Image bauen
```
docker build -t [IMAGENAME] [PFAD]
```
PFAD muss zum Ordner der Dockerfile zeigen.
Innerhalb der Dockerfile befinden sich die Informationen wie das Image gebaut werden soll.

---

## Dockerfile
Die Dockerfile muss genau **Dockerfile** als Datei heißen.

### Quell-Image
```
FROM [IMAGE]
```

### Files in Image kopieren
```
COPY [SRC] [DEST]
```
Wobei SRC immer auf lokaler Maschine und DEST in das Image zeigt.

### Befehl beim starten des Containers
```
CMD [COMMAND]
```
Wird ausgeführt wenn das gebaute Image mit **docker run** ausgeführt wird.

## Weiterführende Doku
Das oben genannte sind die rudimentären Basics. Für weiterführende Befehle und ausführliche Dockfile Syntax sind folgende Quellen sinnvoll:

CLI: https://docs.docker.com/engine/reference/commandline/cli/

Dockerfile: https://docs.docker.com/engine/reference/builder/
